Installation with docker
------------
Inside the project directory:
* Run project with docker : `docker-compose up -d`
* Install dependencies : `docker-compose exec app composer install`
* Execute migrations : `docker-compose exec app php bin/console doctrine:migration:migrate`
* Install yarn : `yarn install`
* Install project assets : `yarn encore dev`

Front access
-------------
* https://app.localhost

Backoffice access
-----------------

* https://app.localhost/bo/login
* login/password: admin@admin.com/admin







