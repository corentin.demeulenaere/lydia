require('../../css/app.css');
require('../../css/global.scss');
require('../../css/bo/request.css');
import 'datatables.net-dt/css/jquery.dataTables.css'

require('bootstrap');
window.$ = window.jQuery = require('jquery');
require('datatables.net');
//require('datatables.net-dt');

$(document).ready(function () {
    $('#table-request').DataTable();

    $('#table-request td.status').each((i, element) => {
        const elt  = $(element);
        const uuid = elt.data('uuid');

        $.ajax({
                   url    : '/bo/request/status',
                   data   : {uuid: uuid},
                   success: function (response) {
                        if(response.state) {
                            elt.html(response.state);
                        }
                   }
               })
    })

});
