<?php

namespace App\Controller\BackOffice;

use App\Entity\Request;
use App\Enum\StateRequestEnum;
use App\Service\Api\Lydia;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RequestController
 *
 * @package App\Controller\BackOffice
 */
class RequestController extends AbstractController
{
    //region Public Methods

    /**
     * @Route("/bo/request", name="bo_request")
     *
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EntityManagerInterface $em)
    {

        $requests = $em->getRepository(Request::class)->findAll();

        return $this->render(
            'backoffice/request/index.html.twig',
            [
                'requests' => $requests,
            ]
        );
    }

    /**
     * @Route("/bo/request/status", name="bo_request_status")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param EntityManagerInterface                    $em
     * @param Lydia                                     $lydia
     *
     * @return JsonResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function status(\Symfony\Component\HttpFoundation\Request $request, EntityManagerInterface $em, Lydia $lydia)
    {
        try {

            $response = $lydia->stateRequest(getenv('VENDOR_TOKEN'), $request->get('uuid'));

            if (!empty($response['error'])) {
                throw new \Exception($response['message']);
            }

            return new JsonResponse(['state' => StateRequestEnum::getLabel($response['state'])]);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()]);
        }
    }
    //endregion Public Methods
}
