<?php

namespace App\Controller;

use App\Entity\Request;
use App\Form\RequestType;
use App\Service\Api\Lydia;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{

    //region Public Methods
    /**
     * @Route("/", name="default")
     *
     * @param EntityManagerInterface                    $em
     * @param \Symfony\Component\HttpFoundation\Request $httpRequest
     * @param Lydia                                     $lydia
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function index(EntityManagerInterface $em, \Symfony\Component\HttpFoundation\Request $httpRequest, Lydia $lydia)
    {

        $form = $this->createForm(RequestType::class, new Request());

        $form->handleRequest($httpRequest);

        if ($form->isSubmitted() && $form->isValid()) {

            try {

                /** @var Request $request */
                $request  = $form->getData();
                $amount   = rand(1, 2000);
                $params   = [
                    'amount'       => $amount,
                    'currency'     => getenv('CURRENCY'),
                    'type'         => 'email',
                    'vendor_token' => getenv('VENDOR_TOKEN'),
                    'recipient'    => $request->getEmail(),
                ];
                $response = $lydia->doRequest($params);

                if (!empty($response['error'])) {
                    throw new \Exception($response['message']);
                }

                $request->setCreatedAt(new \DateTime())
                        ->setUpdatedAt(new \DateTime())
                        ->setAmount($amount)
                        ->setRequestId($response['request_id'])
                        ->setRequestUuid($response['request_uuid'])
                        ->setMobileUrl($response['mobile_url'])
                        ->setMessage($response['message'])
                        ->setStatus(Request::STATE_WAITING);

                $em->persist($request);
                $em->flush();
                $this->addFlash('success', 'Montant : '.$amount.'€. '.$response['message']);

                $form = $this->createForm(RequestType::class, new Request());
            } catch (\Exception $e) {
                $this->addFlash('error', sprintf('Error : %s', $e->getMessage()));
            }
        }

        return $this->render(
            'default/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
    //endregion Public Methods
}
