<?php
/**
 * Created by PhpStorm.
 * User: cdemeulenaere
 * Date: 19/09/2019
 * Time: 09:21
 */

namespace App\Enum;

class StateRequestEnum
{

    //region Constants
    const STATE_ACCEPTED = 'ACCEPTED';
    const STATE_CANCELED = 'CANCELED';
    const STATE_REFUSED  = 'REFUSED';
    const STATE_UNKNOWN  = 'UNKNOWN';
    const STATE_WAITING  = 'WAITING';
    //endregion Constants

    //region Getters/Setters
    /**
     * @return array
     */
    public static function getAll()
    {
        return [
            0  => self::STATE_WAITING,
            -1 => self::STATE_UNKNOWN,
            1  => self::STATE_ACCEPTED,
            5  => self::STATE_REFUSED,
            6  => self::STATE_CANCELED,
        ];
    }

    /**
     * @param $id
     *
     * @return string|null
     */
    public static function getLabel($id)
    {
        $data = self::getAll();

        return $data[$id];
    }

    /**
     * @param $const
     *
     * @return string|null
     */
    public static function getId($const)
    {
        $data = self::getAll();

        return current(array_keys($data, $const));
    }
    //endregion Getters/Setters
}