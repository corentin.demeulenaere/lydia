<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190918150557 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO user (email, password) VALUE (\'admin@admin.com\', \'21232f297a57a5a743894a0e4a801fc3\')');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM user WHERE email=\'admin@admin.com\'');
    }
}
