<?php

namespace App\Service\Api;

use Symfony\Component\HttpClient\HttpClient;

/**
 * Class Lydia
 *
 * @package App\Service\Api
 */
class Lydia
{
    //region Constants
    /**
     *
     */
    private const DO_REQUEST    = '/api/request/do.json';
    /**
     *
     */
    private const STATE_REQUEST = '/api/request/state.json';
    //endregion Constants
    //region Private Properties
    /**
     * @var
     */
    private $vendor;
    /**
     * @var \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    private $httpClient;
    /**
     * @var
     */
    private $baseUrl;
    //endregion Private Properties

    //region Initialization
    /**
     * Lydia constructor.
     *
     * @param $baseUrl
     */
    public function __construct($baseUrl)
    {
        $this->httpClient = HttpClient::create(
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );
        $this->baseUrl    = $baseUrl;
    }
    //endregion Initialization

    //region Public Methods
    /**
     * @param $params
     *
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function doRequest($params)
    {
        try {
            $response = $this->httpClient->request('POST', $this->baseUrl.self::DO_REQUEST, ['body' => $params]);

            return $response->toArray();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $requestUuid
     * @param $vendorToken
     *
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function stateRequest($vendorToken, $requestUuid) {
        try {
            $response = $this->httpClient->request('POST', $this->baseUrl.self::STATE_REQUEST, ['body' => ['request_uuid' => $requestUuid, 'vendor_token' => $vendorToken]]);

            return $response->toArray();
        } catch (\Exception $e) {
            throw $e;
        }
    }
    //endregion Public Methods

}